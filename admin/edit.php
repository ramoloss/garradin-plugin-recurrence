<?php

namespace Paheko;

use Paheko\Entities\Search as SE;
use Paheko\Search;
use Paheko\Users\Session;

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_WRITE);

$recherche = new Search;
$target = 'compta';
$search_url = PLUGIN_ADMIN_URL . 'edit.php';
$user = $session->getUser();

const CURRENT_SEARCH_TARGET = SE::TARGET_ACCOUNTING;

$db = DB::getInstance();

$id = (int) qg('id');
$ok = qg('ok') ?? null;
$result = $sql_query = $search = null;
$is_unprotected = false;
$query = (object) [
	'query' => null
];

// Search existante
if ($id)
{
	$search = $recherche->get($id);

	if (!$search) {
		throw new UserException('Search inconnue ou invalide');
	}

	if ($search->type != SE::TYPE_JSON) {
		if ($search->type == SE::TYPE_SQL_UNPROTECTED) {
			$is_unprotected = true;
		}

		$sql_query = $search->content;
	}
	else {
		$sql_query = $search->content;
		//$query->limit = (int) f('limit') ?: $query->limit;
    }


    if ($sql_query) { // Search SQL
        $sql = $sql_query;
    }
    else { // Search avancée
        $sql = $recherche->buildQuery($target, $query->query, $query->order, $query->desc, $query->limit);
    }

    // Execute search
    $result = $search->query();

    $tpl->assign('result_header', $search->getHeader());
    $tpl->assign('count', $search->countResults(false));

}
else
{
    // Si pas de recherche sélectionnée, renvoyer vers la liste
    $tpl->assign('liste', Search::list(CURRENT_SEARCH_TARGET, Session::getUserId()));
}

$tpl->assign(compact('id', 'target', 'search_url', 'user', 'result', 'ok'));

$tpl->display(PLUGIN_ROOT . '/templates/edit.tpl');
