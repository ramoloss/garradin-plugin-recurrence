<?php

namespace Paheko;

use Paheko\Plugin\Recurrence\Recurrence;

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_READ);

$rec = new Recurrence;

$favs = $rec->listAll();

if (!empty($favs))
{
    $liste = [];
    $where = 't.id IN ('.implode(', ', $favs).')';

    $sql = sprintf('SELECT
        t.id_year, l.id_account, l.debit, l.credit, t.id, t.date, t.reference,
        l.reference AS line_reference, t.label, l.label AS line_label,
        a.label AS account_label, a.code AS account_code
        FROM acc_transactions t
        INNER JOIN acc_transactions_lines l ON l.id_transaction = t.id
        INNER JOIN acc_accounts a ON l.id_account = a.id
        WHERE %s ORDER BY t.id;', $where);

    $transaction = null;
    $db = DB::getInstance();

    foreach ($db->iterate($sql) as $row)
    {
        
        if (null !== $transaction && $transaction->id != $row->id) {
            $liste[] = $transaction;
            $transaction = null;
        }
        
        if (null === $transaction) {
            $transaction = (object) [
                'id'        => $row->id,
                'label'     => $row->label,
                'date'      => \DateTime::createFromFormat('Y-m-d', $row->date),
                'reference' => $row->reference,
                'lines'     => [],
            ];
        }
        
        $transaction->lines[] = (object) [
            'account_label' => $row->account_label,
            'account_code'  => $row->account_code,
            'label'         => $row->line_label,
            'reference'     => $row->line_reference,
            'id_account'    => $row->id_account,
            'credit'        => $row->credit,
            'debit'         => $row->debit,
            'id_year'       => $row->id_year,
        ];
    }

    $liste[] = $transaction;

    $tpl->assign('journal', $liste);
}


$tpl->display(PLUGIN_ROOT . '/templates/index.tpl');