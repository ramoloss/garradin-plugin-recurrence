<?php
namespace Paheko;

$db = DB::getInstance();

$db->import(dirname(__FILE__) . "/data/schema.sql");