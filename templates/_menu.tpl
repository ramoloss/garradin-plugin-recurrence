<nav class="tabs">
    <ul>
        <li{if $current == 'index'} class="current"{/if}><a href="{plugin_url file=""}">Écritures favorites</a></li>
        {if $session->canAccess($session::SECTION_ACCOUNTING, $session::ACCESS_WRITE)}
        <li{if $current == 'edit'} class="current"{/if}><a href="{plugin_url file="edit.php"}">Ajouter des favoris</a></li>
        {/if}
    </ul>
</nav>